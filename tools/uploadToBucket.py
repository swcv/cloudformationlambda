
import sys
import getopt, sys

try:
    import boto3
except ImportError as e:
    print ("Unable to import boto3. Did you forget to pip3 install boto3, or activate vritualenv?!")
    raise(e)

def main():
    def usage():
        print ("USAGE: %s -b <bucket> -f <filename> -r <region> -p <awsprofile>" % sys.argv[0])
    try:
        opts, args = getopt.getopt(sys.argv[1:], "b:f:r:p:", ["bucket=", "file=", "region=", "awsprofile="])
    except getopt.GetoptError as err:
        print (repr(e))
        usage()
        sys.exit(1)
    bucket  = ''
    file    = ''
    region  = ''
    profile = ''
    for o, a in opts:
        if o in ("-b", "--bucket"):
            bucket = a
        elif o in ("-f", "--file"):
            file = a
        elif o in ("-r", "--region"):
            region = a
        elif o in ("-p", "--profile"):
            profile = a
        else:
            print ("Unhandled option %s" % o)
            sys.exit(0)

    if any([a=='' for a in [bucket, file, region]]):
        print("Missing parameters!")
        usage()
        sys.exit(1)

    session = boto3.Session(profile_name=profile)
    s3 = session.client('s3')
    try:
        s3.create_bucket(Bucket=bucket, CreateBucketConfiguration={'LocationConstraint': region})
        print ("Created bucket %s" % bucket)
    except Exception as e:
        print ("Bucket %s exists" % bucket)
        print(repr(e))

    try:
        print ("Uploading to S3 bucket...")
        s3.upload_file(file, bucket, file)
        print ("%s uploaded successfully" % file)
    except Exception as e:
        print ("Failed to upload %s" % file)
        print(repr(e))
        sys.exit(1)

if __name__ == "__main__":
    main()
    sys.exit(0)
