
import json
import subprocess
import time

class LambdaUpdater():

    def __init__(self):
        pass

    def listAllLambdas(self, region='eu-west-1'):
        a = [ 'aws',
              '--region',
              '%s' % region,
              'lambda',
              'list-functions'
            ]

        try: result = json.loads(subprocess.check_output(a))
        except Exception as e:
            print(repr(e))
            raise(e)

        r = []
        for f in result['Functions']:
            #if f['FunctionName'][:len(target)] == target:
            r.append(f['FunctionName'])
        return r

    def updateLambda(self, _f, _zip, _bucket, region='eu-west-1'):

        a = [
            'aws',
            '--region',
            '%s' % region,
            'lambda',
            'update-function-code',
            '--function-name',
            '%s' % _f,
            '--s3-bucket',
            '%s' % _bucket,
            '--s3-key',
            '%s' % _zip
        ]
        print("Updating %s..." % _f)
        try:
            r = json.loads(subprocess.check_output(a))
        except Exception as e:
            print ("Subprocess terminated!")
            raise (e)

    def update(self, zip, bucket, lambdaList=[]):
        try:
            if lambdaList == []:
                lambdaList = self.listAllLambdas()
            #print (lambdaList)
            for _l in lambdaList:
               self.updateLambda(_l, zip, bucket)

            print ("Lambdas updated")
        except KeyboardInterrupt:
            print ("CTRL-C")


if __name__ == "__main__":
    import sys
    import time
    import getopt

    lambdaList      = []
    lambdaZip       = None
    lambdaBucket    = None

    def usage():
        print ("%s [options]" % sys.argv[0])
        print ("  -l|--lambda <name>         : Name of function to update (option can be given multiple times)")
        print ("  -b|--bucket                : Name of the bucket from where to load lambda code zip")
        print ("  -z|--zip                   : Filename of the lambda code zip")
        print ("  -L|--list                  : List all current lambda functions")

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "l:Lb:z:",
                                   ["lambda=", "--list", "--bucket=", "--zip="])
    except getopt.GetoptError:
        usage()
        sys.exit(0)

    c = LambdaUpdater()

    for o, a in opts:
        if o in ("-l", "--lambda"):
            lambdaList.append(str(a))
        if o in ("-b", "--bucket"):
            lambdaBucket = str(a)
        if o in ("-z", "--zip"):
            lambdaZip = str(a)
        if o in ("-L", "--list"):
            print(c.listAllLambdas())
            sys.exit(0)

    if None in [ lambdaList, lambdaZip, lambdaBucket ]:
        usage()
        sys.exit(0)

    c.update(zip=lambdaZip, bucket=lambdaBucket, lambdaList=lambdaList)
