
import os
import requests
import socket
import random
import struct 
import psycopg2

# This is the main method which is invoked by the lambda engine:
#
def lambda_handler(event, context):

    # Resolve our current public IP, just for fun:
    #
    _ip = __resolveIP()
    print("My current IP is '%s'" % _ip)

    # Lambda function can have runtime variables which are assigned in the lambda control panel
    # Lambda engine maps those into environment variables, which can be read using os.environ[] dictionary
    #
    try:
        _target_ip = os.environ['ip']
        _port = int(os.environ['port'])
        _mode = os.environ['mode']
    except Exception as e:
        print("Error in reading parameters: '%s'" % repr(e))
        return { "result": "ERROR" }

    # Output the current params:
    #
    print("My params are: target_ip=%s, target_port=%d, operation_mode=%s" % (_target_ip, _port, _mode))

    # And now that all is good, execute our actual code:
    #
    perform_scan(_mode, _target_ip, _port)

    # Note, there is not really any specific end result that needs to be returned to Lambda engine
    # In this case we just want to return nice result object:
    # 
    return { "result": "OK" }

# This method resolves our current public IP address
#
def __resolveIP():
    r = requests.get("https://ifconfig.co/ip")
    return r.text.strip()

# perform_scan() is our actual test case, performing udp or icmp knocking:
#
def perform_scan(_mode, _target_ip, _port):

    # UDP scan sends a dummy UDP packet to specific IP and port. End result it caught
    # via exception:
    #
    if _mode == 'udp':
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            r = sock.connect((_target_ip, _port))
            if r == 0: result = True
            sock.close()
            print ("UDP scan successful. %s:%d is OPEN" % (_target_ip, _port))
        except Exception as e:
            print("UDP scan failed. %s:%d is CLOSED (reason: %s" % (_target_ip, _port, repr(e)))

    # ICMP scan is performed by RAW sockets. The actual test code is taken from an example here
    # https://gist.github.com/pklaus/856268
    #
    elif _mode == 'icmp':
        ICMP_CODE = socket.getprotobyname('icmp')
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, ICMP_CODE)
            packet_id = int((id(1) * random.random()) % 65535)
            packet = create_packet(packet_id)
            while packet:
                # The icmp protocol does not use a port, but the function
                # below expects it, so we just give it a dummy port.
                sent = sock.sendto(packet, (_target_ip, 1))
                packet = packet[sent:]
            sock.close()
            print("ICMP send successful. %s" % (_target_ip))
        except Exception as e:
            print ("ICMP send failed. %s (reason: %s)" % (_target_ip, repr(e)))

# Following method is from an example: https://gist.github.com/pklaus/856268
#
def checksum(source_string):
    # I'm not too confident that this is right but testing seems to
    # suggest that it gives the same answers as in_cksum in ping.c.
    sum = 0
    count_to = (len(source_string) / 2) * 2
    count = 0
    while count < count_to:
        this_val = source_string[count + 1]*256+source_string[count]
        sum = sum + this_val
        sum = sum & 0xffffffff # Necessary?
        count = count + 2
    if count_to < len(source_string):
        sum = sum + source_string[len(source_string) - 1]
        sum = sum & 0xffffffff # Necessary?
    sum = (sum >> 16) + (sum & 0xffff)
    sum = sum + (sum >> 16)
    answer = ~sum
    answer = answer & 0xffff
    # Swap bytes. Bugger me if I know why.
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer

# Following method is from an example: https://gist.github.com/pklaus/856268
#
def create_packet(id):
    ICMP_ECHO_REQUEST = 8
    """Create a new echo request packet based on the given "id"."""
    # Header is type (8), code (8), checksum (16), id (16), sequence (16)
    header = struct.pack('bbHHh', ICMP_ECHO_REQUEST, 0, 0, id, 1)
    data = bytes((192 * 'Q').encode('utf-8'))
    # Calculate the checksum on the data and the dummy header.
    my_checksum = checksum(header + data)
    # Now that we have the right checksum, we put that in. It's just easier
    # to make up a new header than to stuff it into the dummy.
    header = struct.pack('bbHHh', ICMP_ECHO_EQRUEST, 0,
                         socket.htons(my_checksum), id, 1)
    return header + data

# When this code is run locally the __name__ will be '__main__'
# but when its executed by Lamnda service, it will be something different.
# Hence, local unit test cases can be embedded inside this code block:
#
if __name__ == "__main__":
    print ("Running a local testcase!")
    print(__resolveIP())
    perform_scan('udp', '127.0.0.1', 12345)
    perform_scan('icmp', '127.0.0.1', 12345)
