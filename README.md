
# Cloudformation Lambda example

This example project shows how to setup a custom Lambda method, create an IAM role for it and manage all components with Cloudformation. The example implements following (simple) archtectural solution:

![Architecture](https://bitbucket.org/swcv/cloudformationlambda/raw/master/graphml/arch.png "Sample Architecture")

# Components of the solution

* IAM Role: This document dscribes the basic capabilities which are assigned to the Lambda function. In this case we use AWS managed role resource LambdaBasicExecutionRole
* Lambda function: This is a python 3.6 program designed to run in AWS lambda python sandbox
* Cloudformation: The whole infrastructure managing all the defined resources.

In addition to the above there are a few tools in this package for updating lambda code, uploading code to S3 buckets, etc. Just to ease living. How to use it then...?

# Detailed instructions:

First and foremost: all commands and tools in this package are accessed via command line. In order for them to work, AWS credentials must be in place. AWS cli tools read these credentials from a file in your home directory: ~/.aws/credentials. The content of the file should look like this:

``` bash
[awsprofile]
region=eu-west-1
aws_access_key_id = <accesskeyhere>
aws_secret_access_key = <secretkeyhere>
```

The accesskey and secret access keys can be generated in AWS control panel IAM section. In addition to this, the regular AWS cli tools must be available. In Ubuntu linux you would do as follows (please adjust according to the distribution you use)

``` bash
$ apt-get install awscli
```

Now, the tools and credentials are in place. This toolset is primarily written in python3 so it makes sense to work in a local virtualenv to avoid messing up with system settings. The provided Makefile assists in creating one. To create a new environment and to activate it, issue the following commands:

``` bash
$ make virtualenv
$ source env/bin/activate
```

At this point it might make sense to run your lambda code locally before uploading it into AWS service. As the virtualenv will be the same as in Lambda service, and because the env is now activated, you can run and test the lambda function by running directly:

``` bash
$ python3 lambda/lambda_handler.py
```

Once the code is as you think it should be, you are ready to upload it into lambda service. There are following make targets to suit your needs:

``` bash
$ make compresslambda    # This target creates a ZIP file which is ready to be deployed to AWS lambda service
$ make uploadlambda      # This target uploads created ZIP package into S3 from where Lambda service picks it up
$ make createstack       # This target invokes CloudFormation and asks it to create the complete infrastructure
$ make deletestack       # This target deletes the currently deployed infrastructure
$ make checkstack        # This target checks the current status of the deployed infrastructure
$ make fulldeployment    # A collective target to run: compresslambda, uploadlambda and createstack
```

Please note two important things about the deployment

* The makefile wants to upload ZIP file into a specific bucket in S3. As S3 bucket names are globally unique this most likely does not work for you (because the bucket name is already taken). Please, change the bucket name in the Makefile into something unique which you own.
* Once the complete stack is running it does not make sense to tear it down just when you update your lambda code. See below how only update the code when you make changes.

``` bash
# First list the names of Lambda functions you have deployed:
$ python3 tools/updateLambdaFunction.py -L
['cloudformation-lambda-LambdaFunction-X1QOHH8BRR0M']

# Then, use the lambda name to update the code only:
$ make compresslambda uploadlambda
$ python3 tools/updateLambdaFunction.py -l cloudformation-lambda-LambdaFunction-X1QOHH8BRR0M -b cfdeploymentstorage -z lambda.zip
```

Its not wrong to tear down the stack and re-deploy it via `make fulldeployment` but if it's the code only you upload, this method is faster.

# So, how to run it:

As this example does not provide any integration methods to the Lambda function, it cannot be triggerd outside (like via a REST API or such). Next step is to login into AWS control panel and access the Lambda console. You can see your function there in the list. Click it open, and in the top right corner of the view there is a "TEST" button. Define a dummy test case (the definition can be empty, just add a name for the test case when it asks for it) and then the function can be invoked. You can see the output of the function in the lambda console directly, or if you want to see detailed logs those can be accessed via CloudWatch log service. CloudWatch can be accessed via the "monitorin" button in the lambda function overview.

That's all folks! :)
