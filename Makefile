
PKG=lambda.zip
BUCKET=cfdeploymentstorage
REGION=eu-west-1
NAME=cloudformation-lambda
AWSPROFILE=awsprofile

default:
	@echo "virtualenv:       Create new virtual environment"
	@echo "---"
	@echo "compresslambda:   Compress Lambda snapshot to ${PKG}"
	@echo "uploadlambda:     Upload compressed lambda snapshot to S3"
	@echo "deploystack:      Deploy a new stack using CloudFormation"
	@echo "checkstack:       Check the current status of the stack"
	@echo "deletestack:      Delete the current incarnation of the stack"
	@echo "fulldeployment:   Run: compresslambda, uploadlambda and deploystack"
	@echo "clean:            Delete all autogenerated files"

.PHONY: virtualenv
virtualenv:
	#
	# pre-build virtualenv with python3.6
	# Note, this has proven to work with p36. Some binary packages atm seem
	# to fail with p38. That is the reason why there is python version hardcode here:
	#
	bash -c "if [ ! -d env ]; then \
	  python3.6 -m venv env; \
	  source env/bin/activate; \
	  pip install -r requirements.txt; \
	fi;"
	@echo "Virtual env is installed!"
	@echo "Now, activate it by:"
	@echo " $ source env/bin/activate"

.PHONY: compresslambda
compresslambda:
	#
	# Compress lambda deployment package
	#
	rm -f ${PKG}; \
	cd env/lib/python`python --version | cut -c 8-10`/site-packages; \
	echo "Compressing lambda package..."; \
	zip -r9q ../../../../${PKG} *; \
	cd ../../../../lambda; \
	zip -r9q ../${PKG} *.py

.PHONY: uploadlambda
uploadlambda:
	#
	# Upload deployment package to S3
	#
	python3 ./tools/uploadToBucket.py -b ${BUCKET} -f ${PKG} -r ${REGION} -p ${AWSPROFILE}

.PHONY: deploystack
deploystack:
	#
	# Upload CloudFormation template to S3
	#
	cd cloudformation; \
	python3 ../tools/uploadToBucket.py -b ${BUCKET} -f ${NAME}.json -r ${REGION} -p ${AWSPROFILE}
	#
	# Deploy stack to AWS CloudFormation
	#
	aws cloudformation create-stack \
	  --stack-name ${NAME} \
	  --template-url https://s3-${REGION}.amazonaws.com/${BUCKET}/${NAME}.json \
	  --capabilities CAPABILITY_IAM \
	  --region ${REGION} \
	  --profile ${AWSPROFILE} \
	  --parameters
	@echo "The stack is now in deployment process. It may take a while."
	@echo "Check the current status of the stack by running:"
	@echo "  $ aws cloudformation describe-stacks --stack-name ${NAME}"
	@echo "OR via make target:"
	@echo "  $ make checkstack"

.PHONY: fulldeployment
fulldeployment: compresslambda uploadlambda deploystack
	@echo "Done!"

.PHONY: checkstack
checkstack:
	#
	# Check stack status:
	#
	aws cloudformation describe-stacks --stack-name ${NAME} --region ${REGION} --profile ${AWSPROFILE}
	@echo ""
	@echo "You can delete existing stack by running:"
	@echo "  $ aws cloudformation delete-stacks --stack-name ${NAME}"
	@echo "OR via make target:"
	@echo "  $ make deletestack"

.PHONY: deletestack
deletestack:
	#
	# Delete stack:
	#
	aws cloudformation delete-stack --stack-name ${NAME} --region ${REGION} --profile ${AWSPROFILE}

.PHONY: clean
clean:
	rm -rf ${PKG}
